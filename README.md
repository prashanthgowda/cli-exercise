1: Using "mkdir" to create directory structure and touch command to create empty files

2: Using " find . -name *.txt -type f " command to print all the files with .txt extension from current directory and subdirectories"

3: using " find . -name *.log -delete " command to delete files with .log extension 

4: using echo "GIVEN TEXT" >> hello\one\a.txt to add contents to file a.txt

5: using " zip -r hello.zip hello " to create zip

6: using "cp -r hello world" to copy contents of hello to world

7: using rm -rf hello to delete directory

Harry Potter
1: using curl URL to downlode content to local drive

2: sed -n 1,10p HarryPotter.txt to print first 10 lines of file or use head -n 10 

3: tail -n 10 HarryPotter.txt to print last 10 lines

4: grep -o "Pattern" HarryPotter.txt | wc -l to find the number of occurance 

5: sed -n 100,200p HarryPotter.txt print from line 100 to 200

6: cut -d ' ' -f 1 HarryPotter.txt | sort -u | wc -l to get unique words 
 

Process

ps axo ppid,pid,comm | grep firefox

kill "process id of browser" to kill the browser process

ps axo pcpu,comm | sort -n | tail -3 to print top 

managing software

1: apt-get install <package name> to install a package

2: apt-get purge <package name> to uninstall a package

Misc

1 : curl ifconfig.me  to find my ip address

2 : "ping gogle" or "host google.com"  to look up ip-address 

3 : "whereis python"